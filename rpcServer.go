package mq

import (
	"fmt"
	"strconv"

	"gitee.com/golangx/jsonrpc"
)

// Service 服务
type Service struct {
}

// Push 消息入队列
func (s *Service) Push(j map[string]string, reply *string) error {
	delay, _ := strconv.Atoi(j["delay"])
	TTR, _ := strconv.Atoi(j["TTR"])
	job := &Job{
		ID:    j["id"],
		Body:  j["body"],
		Topic: j["topic"],
		Delay: delay,
		TTR:   TTR,
	}

	err := mq.dispatcher.AddToJobPool(job)
	if err != nil {
		*reply = err.Error()
	} else {
		*reply = "success"
	}
	return nil
}

// Pop 根据topic消费队列
func (s *Service) Pop(topic []string, reply *map[string]string) (err error) {
	*reply, err = Pop(topic...)
	return err
}

// Ack 根据id消费队列
func (s *Service) Ack(id string, reply *bool) (err error) {
	*reply, err = Ack(id)
	return err
}

// RPCServer RPCServer
type RPCServer struct {
}

// Run 启动RPCServer
func (s *RPCServer) Run() {
	server := jsonrpc.NewServer(":9531")
	// server.Register("getUserById", UserInfo)
	// server.Register("getUserExtById", UserExt)
	if err := server.Run(); err != nil {
		fmt.Println(err)
	}
}
