module gitee.com/golangx/mq

go 1.14

require (
	gitee.com/golangx/jsonrpc v0.0.0-20200318131601-56a6c4f0909d
	github.com/gomodule/redigo v2.0.0+incompatible
)
