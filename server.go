package mq

// IServer Server接口
type IServer interface {
	Run()
}

// NewServ 新建Server
func NewServ() IServer {
	var Serv IServer
	Serv = &RPCServer{}
	return Serv
}
